# from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from main.views.common import get_context
from django.http.request import HttpRequest


# после завершения вёрстки добавить обратно
# @login_required(login_url="/login")
def profile_edit_page(request: HttpRequest):
    """
    Создаёт и возвращает страницу профиля пользователя.

    :param request: Запрос пользователя
    :return: Страница профиля пользователя
    """
    context = get_context(request)
    return render(request, "profile.html", context)