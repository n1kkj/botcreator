from django.shortcuts import render
from main.models import Project
from django.http.request import HttpRequest
from main.forms.forms import BotForm
from main.bot_creation.start_bot_code import create_bot
from django.shortcuts import render

from main.forms.forms import BotForm
from main.bot_creation.start_bot_code import create_bot
from django.shortcuts import render


def bot_page(request: HttpRequest, id=-1):
    """
    Создаёт и возвращает страницу бота.

    :param request: Запрос пользователя
    :return: Страница бота
    """
    if id == -1:
        context = {
            'title': 'Ещё не созданный бот',
            'description': 'Станьте первым, кто его увидит!',
        }
    else:
        bot = Project.objects.get(id=id)
        context = {
            'title': bot.title,
            'description': bot.description,
        }
    context['pagename'] = 'Бот | ' + context['title']
    return render(request, "content/bot.html", context)


def create_bot_page(request):
    context = {}
    if request.method == "GET":
        form = BotForm()
    else:
        form = BotForm(request.POST)
        events = str(form.data['events'])
        commands = str(form.data['commands'])
        token = form.data['bot_token']
        if events != '' or commands != '':
            context['bot_str'] = create_bot(events, commands, token)
    context['form'] = form
    return render(request, "create_bot.html", context)
