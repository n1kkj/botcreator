import asyncio
import threading

from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from main.bot_creation.start_bot_code import create_bot
from main.views.common import get_context
import json
from main.bot_creation.eventCommands import events
from main.models import Project
from django.contrib import auth


def next_link(flowchart, links, link, arr):

    l = links[link]["toOperator"]
    if type(l) != str:
        l = str(l)

    if len(flowchart["operators"][l]["properties"]["outputs"]) == 0:
        return arr[:-1] + "\n"
    else:
        arr += flowchart["operators"][l]["properties"]["title"] + " "
        for i, j in links.items():

            k = j["fromOperator"]
            if type(j["fromOperator"]) != str:
                k = str(j["fromOperator"])

            if k == l:
                return next_link(flowchart, links, i, arr)


botstr = ''


def create(flowchart, token, all_events=events):
    raw_str_ev = ''
    raw_str_cmd = ''
    starting_ev = []
    starting_cmd = []
    for i, j in flowchart['operators'].items():
        if len(j["properties"]["inputs"]) == 0:
            if j["properties"]["title"] in all_events.keys():
                starting_ev.append(i)
            else:
                starting_cmd.append(i)
    for i, j in flowchart["links"].items():
        arr = ''
        k = j["fromOperator"]
        if type(k) != str:
            k = str(k)
        if k in starting_ev:
            raw_str_ev += flowchart["operators"][k]["properties"]["title"] + " " + next_link(flowchart,
                                                                                             flowchart["links"], i,
                                                                                             arr).strip() + "\n"
        if k in starting_cmd:
            raw_str_cmd += flowchart["operators"][k]["properties"]["title"] + " " + next_link(flowchart,
                                                                                              flowchart["links"], i,
                                                                                              arr).strip() + "\n"
    bot_str = create_bot(raw_str_ev, raw_str_cmd, token)
    return bot_str


@login_required(login_url="/login")
def bot_editor_page(request):
    context = get_context(request)
    if request.method == "POST":
        form = request.POST
        token = form["bot_token"]
        global botstr
        if "create" in request.POST:
            flowchart = json.loads(form['flowchart'])
            ready_bot_str = str(create(flowchart, token))
            context['bot_str'] = ready_bot_str
            botstr = ready_bot_str
        elif "save" in request.POST:
            name = form["name"]
            desc = form["description"]
            flowchart = json.loads(form['flowchart'])
            ready_bot_str = str(create(flowchart, token))
            bot = Project(name=name, description=desc, bot_str_code=ready_bot_str, user=auth.get_user(request))
            bot.save()
            context['bot_str'] = ready_bot_str
            botstr = ready_bot_str
        elif "start" in request.POST:
            flowchart = json.loads(form['flowchart'])
            ready_bot_str = str(create(flowchart, token))
            start_ds_bot()
            context['bot_str'] = ready_bot_str
            botstr = ready_bot_str
        elif "stop" in request.POST:
            stop_ds_bot()
    else:
        form = request.GET
    context['form'] = form
    return render(request, "boteditor.html", context)


async def exec_discord_bot():
    #exec('x = botstr')
    #await eval('exec(x)')
    await exec(botstr)


def wrap_async_func():
    asyncio.run(exec_discord_bot())


thread_list = []


def start_ds_bot():
    _thread = threading.Thread(target=wrap_async_func)
    thread_list.append(_thread)
    _thread.start()


def stop_ds_bot():
    for i in thread_list:
        i.join(0)
