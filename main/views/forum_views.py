from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from main.forms.forms import QuestionForm, AddAnswerForm
from main.views.common import get_context
from django.http.request import HttpRequest

from main.models import ForumBranch, Message


def forum_page(request: HttpRequest):
    """
    Создаёт и возвращает страницу форума.

    :param request: Запрос пользователя
    :return: Страница форума
    """
    context = get_context(request)
    context['pagename'] = 'Форум'
    context['questions'] = ForumBranch.objects.all
    return render(request, "forum_list.html", context)


def forum_question_page(request: HttpRequest, id: int):
    """
    Создаёт и возвращает страницу вопроса номер id с форума.

    :param request: Запрос пользователя
    :param id: Номер (идентификатор) вопроса
    :return: Страница вопроса на форуме
    """
    context = get_context(request)
    context['pagename'] = 'Форум | Вопрос'
    context['question'] = ForumBranch.objects.filter(id=id)[0]
    context['creation_date'] = context['question'].creation_time.strftime("%d.%m.%Y")
    # print(context['question'].id)
    question_id = context['question'].id

    context['answers'] = Message.objects.filter(forum=ForumBranch.objects.filter(id=question_id)[0])
    context['answers_len'] = context['answers'].count()
    if request.method == 'GET':
        form = AddAnswerForm()
        context['form'] = form
    else:
        form = AddAnswerForm(request.POST)
        if form.is_valid():
            if request.user.is_authenticated:
                question = Message(user=request.user, text=form.data['answerText'],
                                   forum=context['question'])
                question.save()
                return redirect(f"/forum/question/{context['question'].id}")
        else:
            context['error'] = 'Проверьте правильность введенных данных'
        context['form'] = form
    return render(request, "forum_question.html", context)


@login_required(login_url='/login/')
def add_question(request: HttpRequest):
    """
    Создаёт и возвращает страницу добавления вопроса на форум,
    а также добавляет вопрос на форум.

    :param request: Запрос пользователя
    :return: Страница добавления вопроса на форум
    """
    context = get_context(request)
    context['pagename'] = 'Добавить вопрос'
    if request.method == 'GET':
        form = QuestionForm()
        context['form'] = form
    else:
        form = QuestionForm(request.POST)
        if form.is_valid():
            if request.user.is_authenticated:
                question = ForumBranch(title=form.data['title'],
                                       description=form.data['description'],
                                       difficulty=form.data['difficulty'], author=request.user)
                question.save()
                return redirect('/forum')
        else:
            context['error'] = 'Проверьте правильность введенных данных'
        context['form'] = form

    return render(request, 'add_question.html', context)
