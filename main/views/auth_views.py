from django.contrib import auth
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth.models import User

from main.forms.forms import UserForm, LoginForm

from main.models import Profile
from django.http.request import HttpRequest


def get_user_id(request: HttpRequest):
    """
    Получение ID текущего пользователя

    :param request: Запрос пользователя
    :return: ID текущего пользователя
    """
    if auth.get_user(request).id:
        return int(auth.get_user(request).id)


def registration_page(request: HttpRequest):
    """
    Страница регистрации

    :param request: Запрос пользователя
    """
    context = {'pagename': 'Регистрация', 'error': ''}
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            if form.data["password"] == form.data["sec_password"]:
                user1 = User.objects.filter(username=form.data["name"])
                if not user1:
                    user = User(username=form.data["name"])
                    user.set_password(form.data['password'])
                    user.save()
                    new_profile = Profile(fullname=form.data["name"],
                                          status=form.data['status'], age=21,
                                          user=user)
                    new_profile.save()

                    user = authenticate(username=form.data["name"], password=form.data["password"])
                    login(request, user)
                    url = '/profile/' + str(get_user_id(request))
                    return redirect(url)
                else:
                    context["error"] = "Этот юзернейм уже занят! Но Вы всегда можете выбрать другой"
        context["form"] = form
    else:
        context['form'] = UserForm(request.POST)
    return render(request, 'registration.html', context)


def login_page(request: HttpRequest):
    """
    Страница логина

    :param request: Запрос пользователя
    """
    context = {'user_id': get_user_id(request), "pagename": 'Авторизация'}
    if request.method == "GET":
        form = LoginForm()
        context['form'] = form
        if auth.get_user(request).is_authenticated:
            return redirect('/')

    if request.method == "POST":
        if auth.get_user(request).is_authenticated:
            return redirect('/')
        form = LoginForm(request.POST)
        if form.is_valid():
            context['form'] = form
            user = authenticate(username=form.data["username"], password=form.data["password"])
            if user:
                login(request, user)
                return redirect('/')

            redirect('/login')
            context["error"] = "Неправильный логин или пароль"
        else:
            return redirect('/login')

    return render(request, "login.html", context)


def logout_page(request: HttpRequest):
    """
    Выход из аккаунта

    :param request: Запрос пользователя
    :return: Перенаправление на главную страницу
    """
    auth.logout(request)
    return redirect('/')
