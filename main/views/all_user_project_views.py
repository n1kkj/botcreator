from django.shortcuts import render

from django.contrib import auth
from django.http.request import HttpRequest
from main.models import Project


def all_projects(request: HttpRequest):
    """
    Создаёт и возвращает страницу списка всех ботов.

    :param request: Запрос пользователя
    :return: Страница со списком всех ботов
    """
    context = {
        'pagename': 'Все проекты',
        'projects': Project.objects.all(),
    }
    return render(request, 'content/all_projects.html', context)
