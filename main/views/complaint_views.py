from django.shortcuts import render
from django.http.request import HttpRequest

from main.models import ComplaintModel


def complaint_page(request: HttpRequest):
    """
    Создаёт и возвращает страницу отправки жалобы.

    :param request: Запрос пользователя
    :return: Страница отправки жалобы
    """
    context = {}
    context['pagename'] = 'Отправка жалобы'
    return render(request, 'content/complaint.html', context)


def complaint_checker(request: HttpRequest):
    """
    Создаёт и возвращает страницу подтверждения отправки жалобы,
    а также отправляет жалобу.

    :param request: Запрос пользователя
    :return: Страница подтверждения отправки жалобы
    """
    context = {}
    context['pagename'] = 'Жалоба отправлена'
    if request.method == "POST":
        context["content_id"] = request.POST.get("contentid")
        context["reason"] = request.POST.get("reason")
        item = ComplaintModel(bot_name=context["content_id"], reason=context["reason"],
                              status="waiting")
        item.save()
        print(item.reason)
    return render(request, 'content/complaint_complete.html', context)
