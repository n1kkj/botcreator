from django.shortcuts import render
from django.http.request import HttpRequest
from django.contrib import auth

from main.views.common import get_context
from main.views.auth_views import get_user_id


def index_page(request: HttpRequest):
    """
    Создаёт и возвращает главную страницу.

    :param request: Запрос пользователя
    :return: Главная страница
    """
    context = get_context(request)
    if auth.get_user(request).is_authenticated:
        context['id'] = get_user_id(request)
    return render(request, "index.html", context)


def page_not_found(request, exception):
    """
    Создаёт и возвращает страницу с ошибкой 404.

    :param request: Запрос пользователя
    :param exception: Номер ошибки (?)
    :return: Страница с ошибкой
    """
    return render(request, 'error404.html', status=404)

