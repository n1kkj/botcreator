from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from django.http.request import HttpRequest
from main.models import Profile
from main.views.auth_views import get_user_id


@login_required(login_url="/login")
def profile_page(request: HttpRequest, id=-1):
    """
    Создаёт и возвращает страницу профиля пользователя.

    :param request: Запрос пользователя
    :return: Страница профиля пользователя
    """
    if id == -1:
        id = get_user_id(request)
    prof = Profile.objects.get(id=id)
    context = {
        'name': prof.fullname,
        'age': prof.age,
        'status': prof.status,
        'id': id,
        'pagename': "Профиль",
    }
    return render(request, "profile.html", context)
