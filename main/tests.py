from django.test import TestCase, Client

from main.forms.forms import UserForm, LoginForm


class IndexPageTest(TestCase):
    """
    Тестирование главной страницы
    """

    def setUp(self):
        """
        Создание клиента для тестов
        """
        self.client = Client()
        self.response = self.client.get("/")

    def test_working(self):
        """
        Проверка статус-кода страницы
        """
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        """
        Проверка используемого на странице шаблона
        """
        self.assertTemplateUsed(self.response, "index.html")


class ForumlistPageTest(TestCase):
    """
    Тестирование страницы списка вопросов на форуме
    """

    def setUp(self):
        """
        Создание клиента для тестов
        """
        self.client = Client()
        self.response = self.client.get("/forum/")

    def test_working(self):
        """
        Проверка статус-кода страницы
        """
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        """
        Проверка используемого на странице шаблона
        """
        self.assertTemplateUsed(self.response, "forum_list.html")


class ForumAddQuestionPageTest(TestCase):
    """
    Тестирование страницы добавления вопроса на форуме
    (не авторизованными)
    """

    def setUp(self):
        """
        Создание клиента для тестов
        """
        self.client = Client()
        self.response = self.client.get("/forum/add_question/")

    def test_working(self):
        """
        Проверка редиректа на логин
        """
        self.assertEqual(self.response.status_code, 302)


class ProfilePageTest(TestCase):
    """
    Тестирование страницы профиля
    (не авторизованными)
    """

    def setUp(self):
        """
        Создание клиента для тестов
        """
        self.client = Client()
        self.response = self.client.get("/profile/")

    def test_working(self):
        """
        Проверка редиректа на логин
        """
        self.assertEqual(self.response.status_code, 302)


class AllProjectsTest(TestCase):
    """
    Тестирование страницы всех проектов
    """

    def setUp(self):
        """
        Создание клиента для тестов
        """
        self.client = Client()
        self.response = self.client.get("/all_projects/")

    def test_working(self):
        """
        Проверка статус-кода страницы
        """
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        """
        Проверка используемого на странице шаблона
        """
        self.assertTemplateUsed(self.response, "content/all_projects.html")


class RegistrationFormTest(TestCase):
    """
    Тестирование формы регистрации
    """

    def setUp(self):
        """
        Создание общей формы для тестов
        """
        self.form = UserForm()

    def test_name_label(self):
        """
        Тест label'а имени в форме
        """
        self.assertEqual(self.form.fields['name'].label, "Имя")

    def test_status_label(self):
        """
        Тест label'а статуса в форме
        """
        self.assertEqual(self.form.fields['status'].label, "Введите статус")

    def test_sec_password_label(self):
        """
        Тест label'а повторения пароля в форме
        """
        self.assertEqual(self.form.fields['sec_password'].label, "Повторите пароль")

    def test_password_label(self):
        """
        Тест label'а пароля в форме
        """
        self.assertEqual(self.form.fields['password'].label, "Пароль")

    def test_short_password_validity(self):
        """
        Тест валидности данных при вводе короткого пароля
        """
        form_data = {'password': '123'}
        form = UserForm(data=form_data)
        self.assertEqual(form.is_valid(), False)


class LoginFormTest(TestCase):
    """
    Тестирование формы логина
    """

    def setUp(self):
        """
        Создание общей формы для тестов
        """
        self.form = LoginForm()

    def test_name_label(self):
        """
        Тест label'а имени пользователя в форме
        """
        self.assertEqual(self.form.fields['username'].label, "Имя пользователя")

    def test_status_label(self):
        """
        Тест label'а пароля в форме
        """
        self.assertEqual(self.form.fields['password'].label, "Пароль")
