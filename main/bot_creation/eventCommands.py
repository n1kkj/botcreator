"""
Словари ивентов, команд, действий, функций и логических выражений(в разработке)

Первое значение в словаре это текст на русском, а второе это перевод в код
"""


events = {
    'на_сообщение': """on_message(ctx):\n""",
    'присоединился': """on_member_join(ctx):\n""",
}

actions = {
    'прислать_сообщение': """await ctx.channel.send""",
    'ответить': """await ctx.reply""",
    'прислать_закреплённые_смс': """for i in await ctx.channel.pins():
        await ctx.channel.send(i.content) """,
    'удалить_сообщение': """await ctx.delete()""",
    'закрепить_сообщение': """await ctx.message.pin()""",
    'добавить_реакцию': """await ctx.add_reaction""",
    'открепить_все_смс': """for i in await ctx.channel.pins():
        await i.unpin() """,
}

logic_functions = {
    'если_бот': 'if ctx.author.bot:\n',
    'если_не_бот': 'if not ctx.author.bot:\n',
    'если_нет': 'else:\n',
    'конец_если': '',
    'если_cообщение_равно': 'if ctx.content==',
    'если_сообщение_содержит': 'if -1 != ctx.content.find',
    'если_cообщение_не_равно': 'if ctx.content!=',
    'если_сообщение_не_содержит': '-1 == ctx.content.find',
}
