"""
Содержит класс бота и функцию его запуска
"""
from main.bot_creation import create_bot_str
import asyncio
import threading


class Bot:
    """
    Класс бота
    """

    def __init__(self, content):
        self.content = content

    def __str__(self):
        return self.content


def create_bot(event_list, commands_list, token):
    """
    Принимает список ивентов и команд, конвертирует их в бота и возвращает строку
    """
    bot_events = create_bot_str.create_text_events(event_list)
    bot_commands = create_bot_str.create_text_commands(commands_list)
    bot_text = create_bot_str.create_text_bot(bot_events, bot_commands, token)
    bot = Bot(bot_text)
    print(str(bot))
    return bot
