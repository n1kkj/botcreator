"""
Функции создания итогового кода бота, всех ивентов и команд
"""
from main.bot_creation.eventCommands import events, actions, logic_functions
from main.bot_creation.discord_token import token


def get_lst(f_list, raw_string):
    """
    Функция для подбора всех элементов, которые есть в списке переводчика
    """
    return [act for act in f_list.keys() if act in raw_string]


def create_text_bot(events_str: str, commands_str: str, bot_token=token):
    """
    Создаёт итоговый текстовый код бота
    """
    with open("main/bot_creation/discord_code.txt") as f:
        return f.read().format(events_str, commands_str, bot_token)


def translate(i, res):
    raw_str_actions = ' '.join(i.split()[1:]).split()
    # Перебираем все действия
    logic_tab = 4
    for action in raw_str_actions:
        raw_action = action

        for act in get_lst(actions, action):
            action = action.replace(act, (logic_tab * ' ') + actions[act], 1)
        if action.find('):') != -1:
            action = action.replace('):', "):\n", 1)
        else:
            action = action.replace(')', ")\n", 1)

        for act in get_lst(logic_functions, action):
            if logic_functions[act].strip() == 'else:':
                action = action.replace(act, ((logic_tab - 4) * ' ') + logic_functions[act], 1)
            elif logic_functions[act].strip() == '':
                action = action.replace(act, logic_functions[act], 1)
                logic_tab -= 4
            else:
                action = action.replace(act, (logic_tab * ' ') + logic_functions[act], 1)
                logic_tab += 4
        res = res.replace(raw_action, action)

    return res


def create_text_events(event_list: str):
    """
    Принимает список ивентов и возвращает строчки кода

    pattern_ev: паттерн ивента
    """
    if event_list.strip() != '':
        rez_events = ''
        pattern_ev = """@bot.event
async def {}"""
        event_list = event_list.strip().split('\n')
        # Получаем список всех ивентов
        for ev in event_list:

            # Подготавливаем переменные
            res = ev.replace(ev.split()[0], events[ev.split()[0]]).replace(' ', '')

            res = translate(ev, res)
            rez_events += pattern_ev.format(res)
        return rez_events
    return ''


def create_text_commands(commands_list):
    """
    Принимает список команд и возвращает строчки кода

    pattern_cmd: паттерн команды
    """
    if commands_list.strip() != '':
        rez_commands = ''
        pattern_cmd = """@bot.command(name='{}')
async def cmd_{}(ctx):
{}"""
        commands_list = commands_list.strip().split('\n')
        # Получаем список всех команд
        for cmd in commands_list:

            # Подготавливаем переменные
            res = ' '.join(cmd.split()[1:]).replace(' ', '')

            res = translate(cmd, res)
            rez_commands += pattern_cmd.format("cmd_" + cmd.split()[0], cmd.split()[0], res)

        return rez_commands
    return ''
