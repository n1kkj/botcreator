from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    """
    Модель профиля пользователя

    :param fullname: Полное имя пользователя
    :param age: Возраст пользователя
    :param status: Статус пользователя
    :param user: Юзер, к которому привязывается профиль
    """
    fullname = models.CharField(max_length=127)
    age = models.IntegerField(null=True)
    status = models.CharField(null="Я здесь недавно!", max_length=255)
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)


class Project(models.Model):
    """
    Модель проекта

    :param user: Пользователь, который создал бота
    :param picture: Картинка бота
    :param name: Имя бота
    :param description: Описание бота
    :param bot_str_code: Код бота
    """
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    picture = models.ImageField(blank=True)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=512)
    bot_str_code = models.CharField(max_length=40000, default='')


class Functions(models.Model):
    """
    Модель функций бота

    :param name: Название/имя бота
    :param desc: Описание (чего?)
    :param params:
    :param result:
    """
    name = models.CharField(max_length=256)
    desc = models.CharField(max_length=1024)
    params = models.CharField(max_length=256)
    result = models.CharField(max_length=256)


class ForumBranch(models.Model):
    """
    Модель вопроса на форуме

    :param tag: Теги
    :param title: Заголовок вопроса
    :param description: Описание вопроса
    :param difficulty: Сложность вопроса
    :param author: Автор вопроса
    :param creation_time: Время создания вопроса
    """
    QUESTIONS_DIFFICULTY = (
        ('easy', 'Лёгкий'),
        ('middle', 'Средний'),
        ('hard', 'Сложный')
    )

    #tag = models.CharField(blank=True, max_length=500)  Если ничего не сломается, то можно удалить
    title = models.CharField(max_length=500)
    description = models.CharField(max_length=500)
    difficulty = models.CharField(verbose_name='Сложность вопроса', max_length=7, default="easy",
                                  choices=QUESTIONS_DIFFICULTY)
    author = models.ForeignKey(to=User, on_delete=models.CASCADE)
    creation_time = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    """
    Модель ответа на вопрос на форуме

    :param user: Пользователь, который оставил ответ
    :param text: Ответ пользователя
    :param forum: Вопрос, к которому относится ответ
    """
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    forum = models.ForeignKey(to=ForumBranch, on_delete=models.CASCADE)
    # replied_to = models.IntegerField()


class ComplaintModel(models.Model):
    """
    Модель жалобы на бота

    :param bot_name: Название бота
    :param reason: Причина удаления
    :param status: Статус рассмотрения
    """
    bot_name = models.IntegerField()
    reason = models.CharField(max_length=40)
    status = models.CharField(max_length=20)
