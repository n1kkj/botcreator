from django import forms


class UserForm(forms.Form):
    """
    Форма регистрации

    :param name: Имя пользователя
    :param status: Статус пользователя
    :param password: Пароль пользователя
    :param sec_password: Пароль для проверки
    """
    name = forms.CharField(label="Имя", required=True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите ваше имя',
    }))
    status = forms.CharField(label="Введите статус", required=True, widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Что к вас на уме?',
    }))
    password = forms.CharField(label="Пароль", min_length=8, required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите пароль',
    }))
    sec_password = forms.CharField(label="Повторите пароль", required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите пароль ещё раз',
    }))


class LoginForm(forms.Form):
    """
    Форма входа

    :param username: Получаем логин/юзернейм (CharField)
    :param password: Получаем пароль (CharField)
    """
    username = forms.CharField(label="Имя пользователя", required=True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите ваш юзернейм',
    }))
    password = forms.CharField(label="Пароль", required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите пароль',
    }))


class QuestionForm(forms.Form):
    """
    Форма вопроса на форуме

    :param difficulty: Сложность вопроса
    :param title: Название вопроса
    :param description: Описание вопроса
    """
    QUESTIONS_DIFFICULTY = (
        ('easy', 'Лёгкий'),
        ('middle', 'Средний'),
        ('hard', 'Сложный')
    )
    difficulty = forms.ChoiceField(label="Сложность вопроса", choices=QUESTIONS_DIFFICULTY)
    title = forms.CharField(label="Заголовок", required=True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Введите заголовок вопроса',
    }))
    description = forms.CharField(label="Описание", min_length=10, required=True, widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Опишите ваш вопрос',
    }))


class AddAnswerForm(forms.Form):
    """
    Форма добавления ответа на вопрос на форуме

    :param answerText: Текст ответа
    """
    answerText = forms.CharField(label="Ваш ответ", min_length=10, required=True, widget=forms.Textarea(attrs={
        'class': 'form-control w-100 mb-2',
        'placeholder': 'Напишите решение на этот вопрос',
    }))

class BotForm(forms.Form):
    """
    Форма создания бота
    """
    attrs = {
        'class': 'form-control'
    }
    events = forms.CharField(label="Список ивентов", required=False, widget=forms.TextInput(attrs=attrs))
    commands = forms.CharField(label="Список команд", required=False, widget=forms.TextInput(attrs=attrs))
