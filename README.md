# BotCreator


![Group](https://img.shields.io/badge/group-s104-succsess)

## Для чего проект?

Проект создан для того, чтобы сделать разработку дискорд ботов проще и удобнее

## Основная ветка
`develop`

## Стек технологий
[Python](https://www.python.org), [Django 4](https://www.djangoproject.com/), [Pillow](https://pillow.readthedocs.io/en/stable/index.html), [Bootstrap 5](https://getbootstrap.com), [Flowchart js](https://flowchart.js.org), [Discord.py](https://github.com/Rapptz/discord.py)

## Как использовались технологии
- Python - основной язык программирования
- Django - веб фреймворк для создания проекта
- Pillow - для хранения картинок в бд
- Flowchart js - для фронтенд интерфейса создания бота
- Discord.py - для создания бэкенда бота(через discord API)

## Как развернуть
```commandline
git clone https://gitlab.informatics.ru/2022-2023/mytischi/s104/botcreator.git
cd botcreator
git fetch
git checkout develop
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py makemigrations main
python3 manage.py migrate
python3 manage.py runserver
```
(для linux)

## Развернуть через Docker контейнер
```commandline
git clone https://gitlab.informatics.ru/2022-2023/mytischi/s104/botcreator.git
cd botcreator
git fetch
git checkout develop
sudo docker build . -t botcreator
sudo docker run -d -p 8000:8080 botcreator
```

## Запуск тестов
```commandline
python3 manage.py test
```
(для linux)

## Паттерны проектирования
- [MVT(MVC) - Model View Template](https://pythonpip.ru/django/django-mvt)

## Участники
[Список участников](https://gitlab.informatics.ru/2022-2023/mytischi/s104/botcreator/-/project_members)

## Примерные задачи каждого
[Таблица](https://docs.google.com/spreadsheets/d/1lU6HbmkRkM_2cYKGx7lOYrB97_uzZRtUlB3HUMvVjnE/edit#gid=0)
(она на то и примерная, что там не все есть)

## Активность
[Свежие коммиты со всех веток](https://gitlab.informatics.ru/2022-2023/mytischi/s104/botcreator/activity)

## Статус проекта
В активной разработке


Тут может быть ваша реклама

~~где-то я это уже видел...~~