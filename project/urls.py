from django.contrib import admin
from django.urls import path
from main.views import index_views, profile_views
from main.views.index_views import page_not_found
from main.views import  index_views, profile_views, auth_views, forum_views, complaint_views,\
    all_user_project_views, bot_views
from main.views import admin_views, index_views, profile_views, auth_views, bot_views, bot_editor_views

urlpatterns = [
    path('', index_views.index_page, name="Главная"),

    path('registration/', auth_views.registration_page, name="Регистрация"),
    path('login/', auth_views.login_page, name='login'),
    path('logout/', auth_views.logout_page, name="Выйти"),

    path('profile/', profile_views.profile_page, name="Профиль"),
    path('profile/<int:id>/', profile_views.profile_page, name="Чей-то профиль"),

    path('forum/', forum_views.forum_page, name='Форум'),
    path('forum/question/<int:id>', forum_views.forum_question_page),
    path('forum/add_question/', forum_views.add_question, name='Добавить вопрос'),

    path('complaint/', complaint_views.complaint_page, name='complaint'),
    path('complaint_checker/', complaint_views.complaint_checker, name='complaint_checker'),

    path('all_projects/', all_user_project_views.all_projects, name='all_project_pages'),
    path('bot/', bot_views.bot_page, name="Профиль бота"),

    path('admin/', admin_views.index_page, name="Админ. панель"),
    path('bot/editor', bot_editor_views.bot_editor_page, name="Программирование бота"),
]

handler404 = page_not_found
